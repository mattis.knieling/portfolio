//----------- Get 'Menu' Variables ---------------\\
const hamburgerDark = document.getElementById('hamburger');
const hTop = document.getElementById('h-top');
const hMiddle = document.getElementById('h-middle');
const hBtm = document.getElementById('h-btm');
const linksDark = document.querySelectorAll('.links');
const crossDark = document.getElementById('cross');
const crossFirst = document.getElementById('first');
const crossSecound = document.getElementById('secound');
const navContainer = document.getElementById('full-nav');
const topBtnDark = document.getElementById('top-btn')

//----------- Get 'Change Mode' variable ---------------\\

let changeMode = document.getElementById('change-mode');
const skillButton = document.getElementById('skills-button');
const goalButton = document.getElementById('goals-button');



//----------- Get 'Skillbar-Container' Variable -----------\\

const skillbarContainer = document.querySelectorAll('.skillbar');

//------- Get 'ContentBox' Variable ---------------\\

const contentBox = document.querySelectorAll('.content');

//----- set variable for mode check-------\\

let light = false;
console.log(light);

//----- Change to dark mode -----\\

changeMode.addEventListener('click', function(){
    
    

    
        light  ?  document.body.style.backgroundColor = '#fff' : document.body.style.backgroundColor = '#1d1d1d' ;
        light  ?  hamburgerDark.style.backgroundColor = '#1d1d1d' : hamburgerDark.style.backgroundColor = '#ddd';
        light  ?  hTop.style.backgroundColor = '#ddd' : hTop.style.backgroundColor = '#1d1d1d';
        light  ?  hMiddle.style.backgroundColor = '#ddd' : hMiddle.style.backgroundColor = '#1d1d1d' ;
        light  ?  hBtm.style.backgroundColor = '#ddd' : hBtm.style.backgroundColor = '#1d1d1d';
        light  ?  crossFirst.style.backgroundColor ='#ddd' : crossFirst.style.backgroundColor ='#1d1d1d';
        light  ?  crossSecound.style.backgroundColor ='#ddd' : crossSecound.style.backgroundColor ='#1d1d1d';
        light  ?  changeMode.classList.remove('change-mode-dark') : changeMode.className += ' change-mode-dark'
        light  ?  crossSecound.style.backgroundColor ='#ddd' : crossSecound.style.backgroundColor ='#1d1d1d';
        light  ?  skillButton.style.cssText ='background-color: #1d1d1d; color: #ddd;' :skillButton.style.cssText ='background-color: #ddd; color: #1d1d1d;';
        light  ?  goalButton.style.cssText ='background-color: #1d1d1d; color: #ddd;': goalButton.style.cssText ='background-color: #ddd; color: #1d1d1d;';
        light  ?  topBtnDark.style.cssText ='background-color: #1d1d1d; color: #ddd;' : topBtn.style.cssText ='background-color: #ddd; color: #1d1d1d;';
       



      if(light === false){
            for( i = 0 ; i < contentBox.length; i++ ){
                contentBox[i].className += ' dark-content'; 
            };

            for( i = 0 ; i < linksDark.length; i++ ){
                linksDark[i].className += ' dark-links';
            };

            for( i = 0 ; i < skillbarContainer.length; i++ ){
                skillbarContainer[i].className += ' dark-skillbar';
            };
        }


        if(light === true){
            for( i = 0 ; i < contentBox.length; i++ ){
                contentBox[i].classList.remove('dark-content'); 
            };

            for( i = 0 ; i < linksDark.length; i++ ){
                linksDark[i].classList.remove('dark-links');
            };

            for( i = 0 ; i < skillbarContainer.length; i++ ){
                skillbarContainer[i].classList.remove('dark-skillbar');
            };

        }   
        	
        hamburgerDark.addEventListener('click', function(){
            light ? navContainer.style.backgroundColor = '#ddd': navContainer.style.backgroundColor = '#1d1d1d';
            changeMode.style.display = ' none';
        });

        cross.addEventListener('click', function(){
            navContainer.style.backgroundColor = 'transperent';
            changeMode.style.display = ' flex';
        });

        
    if(light !== false){
        light = false;
        
    }else{
        light = true;
    };

    console.log(light);

      
    });  

       